import React from 'react';
const team = [
  {
  id:1,
  name:'Management',
  members:[
    // section:'Management Team',
    {
      id:1,
      name:'Mattia Pintus',
      role:'CEO'
    },
    {
      id:2,
      name:'Giorgio Pintus',
      role:'COO',
    },
    {
      id:3,
      name:'Matteo Cugusi',
      role:'CIO'
    }
  ]},
  {
  id:2,
  name:'Advisors',
  members:[
    {
      id:1,
      name:'Giulia Aranguena',
      role:'Legal Advisor'
    },
    {
      id:2,
      name:'Federico Serratore',
      role:'Legal Advisor'
    },
    {
      id:3,
      name:'Atanas Dinchev',
      role:'Technical Advisor'
    },
    {
      id:4,
      name:'Alberto De Luigi',
      role:'Blockchain Expert'
    },
    {
      id:5,
      name:'Bill Yu',
      role:'Hardware Development'
    }
  ]},
  {
  id:3,
  name:'Development',
  members:[
    {
      id:1,
      name:'David Jaramillo',
      role:'Fullstack and Blockchain developer'
    },
    {
      id:2,
      name:'Federico Calarco',
      role:'Fullstack and Blockchain developer'
    }
  ]},
  {
  id:4,
  name:'Mining Operators',
  members:[
    {
      id:1,
      name:'Pierluigi Bolino',
      role:'Senior Operator'
    },
    {
      id:2,
      name:'Andrea Sechi',
      role:'Junior Operator',
    },
    {
      id:3,
      name:'Davide Damiano',
      role:'Junior Operator'
    },
  ]},
  {
  id:5,
  name:'Social Media Marketing',
  members:[
    {
      id:1,
      name:'Fausto Bonifacio',
      role:'Community Manager'
    },
    {
      id:2,
      name:'Cheung Ho Yi',
      role:'Art Director'
    },
    {
      id:3,
      name:'Stefania Stimolo',
      role:'Copywriter and Translator'
    },
    {
      id:4,
      name:'Farcas Ionut',
      role:'Social Helper'
    },
    {
      id:5,
      name:'Sean Logan',
      role:'Social Helper'
    }
  ]},
  {
  id:6,
  name:'Supporter',
  members:[
    {
      id:1,
      name:'Stefano Casu',
      role:'Crypto trader/analyst'
    },
    {
      id:2,
      name:'Matteo Perticone',
      role:'Support Manager'
    },
    {
      id:3,
      name:'Gabriele Mazzola',
      role:'Support Operator'
    }
  ]}]


export const Team = ({match}) => (
  <div className="row no-gutters d-flex justify-content-center h-100">
    <div className="col-8">

    {
      team.map(team => {
        return(
          <div className="row no-gutters d-flex justify-content-center" key={team.id}>

              <h1 className="col-12 text-center team mont white animated fadeInUp light ">
                {team.name}
              </h1>

                  {team.members.map(member=> {
                    return(
                      <div className="col text-center member animated fadeInDown" key={member.id}>

                        <div className="row-no-gutters intTab" key={member.id}>
                          <p className="col-12 text-center name mont white">{member.name}</p>
                          <p className="col-12 text-center role">{member.role}</p>
                        </div>

                      </div>

                    )
                  })}

          </div>
      )})
    }

    </div>
  </div>
)
