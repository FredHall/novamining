import React from 'react';
import telegram from '../images/telegram.png';

export const Join = ({props}) => (

  <div className="row no-gutters d-flex justify-content-center">

    <div className="col-10 col-sm-5 col-md-3"  onClick={()=> window.location.href ="https://t.me/novaminingitalia"}>
      <div className="row-no-gutters tab animated fadeIn">

          <p className="col-12 text-center">
            Italian group
          </p>
        <div className="col-12">
          <img src={telegram} className="icon animated fadeInLeft col-4 col-sm-auto" alt="logo" />
        </div>

      </div>
    </div>

    <div className="col-10 col-sm-5 col-md-3 tab animated fadeIn"  onClick={()=> window.location.href ="https://t.me/novamininginternational"}>
      <div className="row-no-gutters">

          <p className="col-12 text-center">
            International Group
          </p>
        <div className="col-12">
          <img src={telegram} className="icon animated fadeInRight col-auto" alt="logo" />
        </div>

      </div>
    </div>
  </div>
)
