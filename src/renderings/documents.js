import React from 'react';
import {BrowserRouter as Router , Route , Link} from 'react-router-dom';
import { Join } from './join.js';


export const Documentation = ({match}) => {

          return(
            <Router>

            <div className="row no-gutters d-flex justify-content-center ">

              <h1 className="col-12 mont white text-center animated fadeIn bold">Coming soon</h1>
              <h3 className="col-12 mont white text-center animated fadeIn normal" style={{marginTop:20}}>Our Team is working very hard</h3>
              <p className="col-12 text-center animated fadeIn mont white normal" style={{marginTop:150}}>In a couple of days we will be releasing the whitepaper</p>
              <p className="col-12 text-center animated fadeIn mont white normal">We please you to join our channels of communication for any further instruction.</p>

              <Route
                   exact
                   path="/contacts"
                   render={() =><Link to={`${match.url}/join`} className="mont white col-auto yButton" style={{marginTop:20 , padding:'5px 10px'}}>Contact us</Link>}
                 />


              <div className="col-12 text-center">
                <Route exact path={`${match.url}/join`} component={Join} />
              </div>

            </div>
          </Router>
        )
      };
