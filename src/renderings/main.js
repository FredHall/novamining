import React , { Component } from 'react';
import { BrowserRouter as Router, Route, Link , Switch , Redirect  } from "react-router-dom";
import { database } from '../firebase';
import axios from 'axios';
import { Home } from './home.js';
import { Join } from './join.js';
import { Documentation } from './documents.js';
import { Team } from './team.js';
import Slider from 'rc-slider';
import logo from '../images/Logo_1.png';
import dataCenter from '../images/datacenter.jpg';
import 'rc-slider/assets/index.css';
import '../css/App.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';


class Main extends Component {

  constructor(props){
    super(props);
    this.state = {
      amount:0,
      change:undefined,
      crypto:'',
      data:undefined,
      date: new Date(),
      email:'',
      isMenu:false,
      isSubmitActive: false,
      isSubmitable:true,
      name:'',
      redirect:undefined,
      selectedOption: '',
    }

  }

  handleChange(e){
    var change = {}
      change[e.target.name] = e.target.value
        this.setState(change)
  }

  handleSelect = (selectedOption) => {
    this.setState({ selectedOption });

  }




  submit = () => {

    let email = this.state.email;
      var re = email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        console.log(re);
          let addUser = [{
              name: this.state.name,
                email: this.state.email,
                  amount: this.state.amount,
                    conversion: Number(this.state.amount) / this.state.change,
                      date:  this.state.date.toString(),
                  }]

    console.log(addUser);

    // let v = this.state;
    // const submitable = v.amount.length > 0 && v.crypto.lenght > 0 && re!== null && v.email.lenght > 0;
    //   submitable === true ? this.setState({isSubmitable:true}) : null;

            let data = database.ref()
                              .child('ico/landing/whitelist')
                                .push(addUser ,
                                function(error){
                                  if(error){
                                    console.log(error)
                                  }else{
                                    console.log('data has been saved')
                                    return true;
                                  }
                                  return;
                                })

              }

  componentDidMount(){
    console.log(this.props)
    database.ref().on('value' , (snapshot) => {
          console.log(snapshot.val())
        });

        axios.get('https://api.coinmarketcap.com/v1/ticker/?convert=EUR&limit=10').then(
         function(response){
           return response;
         }
      ).then(data =>{
        console.log(data.data[1].price_usd);
        let price = data.data[1].price_usd;
        data.error ? this.setState({error:'error'}) :
        this.setState({ change: price})
          }).catch(error=>{
            console.log(error);
            throw error;
          });
          console.log('auth')
          const auth = localStorage.getItem('hasAccess?');
          auth !== 'true' ? this.setState({redirect:true}) : null;
          console.log(auth)
  }

  componentDidUpdate(){
    console.log(this.state)
    // this.state.redirect  ? window.location.href = "/Auth" : null;
  }



  render() {

        const { selectedOption } = this.state;
      	const value = selectedOption && selectedOption.value;
        const eth = Number(this.state.amount) / this.state.change;
        const steps = this.state.amount < 10000 ? 500 : this.state.amount < 50000 ? 1000 : 5000;

    return (
    <Router>

      <div className="container-fluid p-0 m-0">

      <div className="p-0 col-12 col-sm-8 col-md-7 col-lg-4 col-xl-3 d-flex form " style={{bottom: this.state. isSubmitActive ? '0' : '-320px' }}>

        <div className="row no-gutters d-flex w-100 h-100 align-self-stretch">
          <button className="col-12 text-center hoverable openContact" onClick={() => this.setState({ isSubmitActive:!this.state. isSubmitActive})} style={{ height:50}}>{this.state. isSubmitActive ? 'Close' : 'Join the whitelist'}</button>

            <form action="" className="col-12 align-self-center" >
              <label htmlFor="" className="col-12">
                  <input type="text" name="email" className="col-12 text-center" value={this.state.email} onChange={this.handleChange.bind(this)} placeholder="Email"/>
                  <input type="text" name="name" className="col-12 text-center" value={this.state.name} onChange={this.handleChange.bind(this)} placeholder="Full name"/>
                  <div className="row no-gutters d-flex h-50 justify-content-center">
                    {/*  <p className="col-4 text-left mont white light align-self-center" style={{marginTop:15}}>Crypto:</p>
                    <Select
                      style={{marginTop:10}}
                       name="form-field-name"
                       value={value}
                       onChange={this.handleSelect}
                       className="col-8  p-0 light mont white"
                       options={[
                         { value: 'one', label: 'Bitcoin' },
                         { value: 'two', label: 'Ethereum' },
                         { value: 'three', label: 'Litecoin' },
                       ]}
                     /> */}
                      <p className="col-12 text-center white light mont">What will be your investment</p>
                           <div className="col-11">
                            <Slider min={0} max={100000} step={steps} onChange={(value) => this.setState({amount:value})}/>
                          </div>
                        <p className="col-12 text-center mont yellow bold text-right">{this.state.amount}$</p>


                      {/*  <input type="number" name="amount" className="col-12 text-center ml-auto" value={this.state.amount} onChange={this.handleChange.bind(this)} placeholder="Amount"/> */}
                    <p className="col-12 text-center mont white bold">{this.state.change !== undefined ? eth.toFixed(2) +' ETH' : 'Conversion not available'}</p>
                  </div>

              </label>

            </form>
            <div className="w-100"></div>
            <div className="col-12 align-self-end m-0 p-0">
              <label htmlFor="" className="col-12">
                <button className="col-12" style={{ cursor : this.state.isSubmitable ? 'pointer' : 'not-allowed' , opacity: this.state.isSubmitable ? 'inherit' : '0.2'  }} onClick={() => this.state.isSubmitable ? this.submit() : null}>Submit</button>
              </label>

            </div>
        </div>
      </div>

        <header className="row no-gutters d-flex justify-content-center">
            <div className="col-12 col-sm-10 col-md-8 col-lg-7" id="navbar">

              <div className="row no-gutters d-flex">

                <img src={logo} className="App-logo animated align-self-center fadeInLeft col-4 col-sm-3 col-md-3 mr-auto"  alt="logo" />
                <button className="col-auto animated fadeInRight col-2 ml-auto d-sm-none align-self-center navbutton" onClick={() => this.setState({isMenu: true})}>Menù</button>


                    <div className="d-none d-sm-block col-sm-auto ml-auto text-right" style={{minHeight:150 , minWidth:'40vw'}}>

                        <div className="row no-gutters d-flex h-100 animated fadeInRight justify-content-end  align-items-center">
                          <div className="col ">&nbsp;</div>

                          <Link to="/" className="hoverable col-auto ">Home</Link>
                          <Link to="/contacts" className="hoverable col-auto ">Documentation</Link>
                          <Link to="/team" className="hoverable col-auto ">Team</Link>
                          <Link to="/contacts/join" className="hoverable col-auto button">Join</Link>

                        </div>

                    </div>

              </div>
            </div>

            <div className="col-12 h-50">

              <div className="row no-gutters d-flex align-self-center justify-content-center h-100">

                  <div className="col-12 text-center h-100">

                      <Switch>


                              <Route  path={`/`} exact  component={Home}  />
                              <Route  path={`/contacts`} component={Documentation}  />
                              <Route  path={'/team'}  component={Team} />

                      </Switch>



                  </div>
              </div>

            </div>

        </header>


        <section className="row no-gutters d-flex justify-content-center about">

            <div className="col-10 h-100">

                  <div className="row no-gutters d-flex justify-content-center h-100" id="about">
                      <div className="col-11 col-sm-12  paper h-100">
                        <div className="row no-gutters d-flex justify-content-center mission h-100" style={{marginTop:50 , marginBottom:50}}>

                            <div className=" col-12 col-md-6 p-0 col-lg-4 align-self-center align-items-center " >

                              <div className="proportioned  p-0 row no-gutters ">

                                  <hr style={{maxWidth:60}} className="col-2 align-self-start"/>
                                  <h1 className="col-12 bold blue text-left ">
                                  Nova has a mission
                                  </h1>

                                  <p className="col-10 text-justify paragraph">
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ipsa earum culpa dolores asperiores! Hic, aperiam voluptates magnam sit natus ut quod architecto adipisci, quaerat excepturi reiciendis mollitia. Sapiente, magnam!
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ipsa earum culpa dolores asperiores! Hic, aperiam voluptates magnam sit natus ut quod architecto adipisci, quaerat excepturi reiciendis mollitia. Sapiente, magnam!
                                  </p>

                                  <p className="col-10 text-justify paragraph">
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ipsa earum culpa dolores asperiores! Hic, aperiam voluptates magnam sit natus ut quod architecto adipisci, quaerat excepturi reiciendis mollitia. Sapiente, magnam!
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ipsa earum culpa dolores asperiores! Hic, aperiam voluptates magnam sit natus ut quod architecto adipisci, quaerat excepturi reiciendis mollitia. Sapiente, magnam!
                                  </p>

                                  <div className="w-100"></div>

                                  <button className="col-sm-6  col-lg-auto mont light f15" style={{padding:5 , marginBottom:10}}>Whitepaper</button>
                                  <button className="col-sm-6  col-lg-auto mont light f15" style={{padding:5 , marginBottom:10}}>Roadmap</button>
                                  <button className="col-sm-6  col-lg-auto mont light f15" style={{padding:5 , marginBottom:10}}>Contribution</button>


                              </div>

                            </div>

                            <div className="proportioned col-12 col-md-6 col-lg-4 background align-self-center h-100" style={{backgroundImage:`url(${dataCenter})`}}>

                            </div>

                        </div>


                      </div>


                  </div>

            </div>


        </section>

        <section className="row no-gutters d-flex align-items-center align-self-center justify-content-center invitation">


        <div className="col-8 col-sm-4 col-md-3 ">
            <div className="row no-gutters d-flex justify-content-center numbers">

                <div className="col-12">
                    <div className="row no-gutters justify-content-center">
                    <p className="col-12 text-center mont white normal f15">Total tokens volume:</p>
                    <p className="col-12 text-center mont white bold f25 yellow">100 Milion XNM</p>
                    </div>
                </div>


            </div>
        </div>

        <div className="col-8 col-sm-4 col-md-3 ">
            <div className="row no-gutters d-flex justify-content-center ">

                <div className="col-12">
                    <div className="row no-gutters justify-content-center">
                    <p className="col-12 text-center mont white normal f15">Soft cap</p>
                    <p className="col-12 text-center mont white bold f25 yellow">57 Milion usd</p>
                    </div>
                </div>


            </div>
        </div>

        <div className="col-8 col-sm-4 col-md-3 ">
            <div className="row no-gutters d-flex justify-content-center ">

                <div className="col-12">
                    <div className="row no-gutters justify-content-center">
                    <p className="col-12 text-center mont white normal f15">Hard cap</p>
                    <p className="col-12 text-center mont white bold f25 yellow">90 Milion usd</p>
                    </div>
                </div>


            </div>
        </div>

        <div className="w-100"></div>

        <hr style={{background:'yellow' , maxWidth:150}} className="col-12"/>

        <div className="w-100"></div>

        <div className="col-11 col-sm-8 col-md-5 col-lg-5" style={{border:'1px solid white'}}>
            <div className="row no-gutters d-flex justify-content-center numbers">

                <div className="col-12 col-sm-8 col-md-6">
                    <div className="row no-gutters justify-content-center d-flex">
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white normal f25">Presale date:</p>
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white bold f25 yellow align-self-center">15.04.17</p>
                    <p className="col-6 text-center mont white light f15">20 Milion Token</p>
                    <p className="col-6 text-center mont yellow bold f15">1.10$</p>

                    </div>
                </div>


            </div>
        </div>

        <div className="col-11 col-sm-8 col-md-5 col-lg-5" style={{border:'1px solid white'}}>
            <div className="row no-gutters d-flex justify-content-center numbers">

                <div className="col-12 col-sm-8 col-md-6">
                    <div className="row no-gutters justify-content-center d-flex">
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white normal f25">Presale date:</p>
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white bold f25 yellow align-self-center">15.04.17</p>
                    <p className="col-6 text-center mont white light f15">20 Milion Token</p>
                    <p className="col-6 text-center mont yellow bold f15">1.10$</p>

                    </div>
                </div>


            </div>
        </div>

        <div className="col-11 col-sm-8 col-md-5 col-lg-5" style={{border:'1px solid white'}}>
            <div className="row no-gutters d-flex justify-content-center numbers">

                <div className="col-12 col-sm-8 col-md-6">
                    <div className="row no-gutters justify-content-center d-flex">
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white normal f25">Presale date:</p>
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white bold f25 yellow align-self-center">15.04.17</p>
                    <p className="col-6 text-center mont white light f15">20 Milion Token</p>
                    <p className="col-6 text-center mont yellow bold f15">1.10$</p>

                    </div>
                </div>


            </div>
        </div>

        <div className="col-11 col-sm-8 col-md-5 col-lg-5" style={{border:'1px solid white'}}>
            <div className="row no-gutters d-flex justify-content-center numbers">

                <div className="col-12 col-sm-8 col-md-6">
                    <div className="row no-gutters justify-content-center d-flex">
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white normal f25">Presale date:</p>
                    <p className="col-6 col-sm-12 col-md-12 col-lg-6 text-center mont white bold f25 yellow align-self-center">15.04.17</p>
                    <p className="col-6 text-center mont white light f15">20 Milion Token</p>
                    <p className="col-6 text-center mont yellow bold f15">1.10$</p>

                    </div>
                </div>


            </div>
        </div>

        <div className="w-100"></div>

        <hr style={{background:'yellow' , maxWidth:150}} className="col-12"/>

        <div className="w-100"></div>

        <div className="col-12" style={{margin:'100px 0'}}>
            <div className="row no-gutters d-flex justify-content-center ">

                <div className="col-12 col-sm-8 col-md-6">
                    <div className="row no-gutters justify-content-center">

                    <p className="col-12 text-center mont white normal f30">Presale starts in:</p>

                    <p className="col-12 text-center mont white bold f33 yellow" id="countdown"> 00:00:00:00</p>

                    <div className="w-100"></div>

                    <hr style={{background:'yellow' , maxWidth:130}} className="col-12"/>

                    <div className="w-100"></div>

                    <button className="darkblue mont bold join">Join</button>

                    </div>
                </div>


            </div>
        </div>



        </section>

        <div className="col-12 mobileMenu h-100 d-sm-none'" style={{display:this.state.isMenu ? 'block' : 'none'}}>
          <div className="row no-gutters align-self-center align-items-center h-50 d-flex justify-content-center">
            <div className="col-10">
              <div className="row no-gutters">
                <button className="col-12 text-center">Home</button>
                <button className="col-12 text-center">Team</button>
                <button className="col-12 text-center">Join</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    </Router>

    );
  }
}
export default Main;
