import React from 'react';

export const Home = (props) => (

  <div className="row no-gutters d-flex justify-content-center homepage">


      <h1 className="col-10 text-center mont white normal">Full-Stack Mining Environment</h1>

        <div className="w-100"></div>
          <h2 className="col-6 text-center mont white light">Mining, trustless. Improved by Proof of Hashrate Commitment.</h2>
            <div className="w-100"></div>

            <div className="col-4 col-sm-2 details ">
              <div className="row no-gutters d-flex align-items-center h-100">
                <h3 className="col-12 text-center mont white bold m-0">Hardware Production</h3>
              </div>
          </div>

          <div className="col-4 col-sm-2 details ">
          <div className="row no-gutters d-flex align-items-center h-100">
                <h3 className="col-12 text-center mont white bold m-0 ">Cloud Mining</h3>
              </div>
          </div>

          <h4 className="col-12 text-center mont white light p-0">Support NovaMining Support Bitcoin.</h4>

        <div className="w-100"></div>

  </div>

  )
