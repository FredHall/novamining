import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch , Redirect } from "react-router-dom";
import history from './renderings/history'
import Login from './renderings/login.js'
import Main from './renderings/main.js';
import { Team } from './renderings/team.js';
import { Documentation } from './renderings/documents.js';

class App extends Component {

  constructor(props){
    super(props);
  }

  componentWillMount(){

    // const auth = localStorage.getItem('hasAccess?');
    // auth === 'true' ? window.location.href="/" : console.log('false')
    // console.log(auth)
  }
//
//   handleChange(e){
//     var change = {}
//       change[e.target.name] = e.target.value
//         this.setState(change)
//   }
//
//   handleSelect = (selectedOption) => {
//     this.setState({ selectedOption });
//
//   }
//
//   componentWillMount(){
//     // temporary
//     var access = localStorage.getItem('hasAccess?');
//         access === 'true' ? this.setState({isRenderable:true}) : this.setState({isRenderable:false});
//         console.log(access);
//     //
//   }
//
//   submit = () => {
//
//     let email = this.state.email;
//       var re = email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
//         console.log(re);
//           let addUser = [{
//               name: this.state.name,
//                 email: this.state.email,
//                   amount: this.state.amount,
//                     conversion: Number(this.state.amount) / this.state.change,
//                       date:  this.state.date.toString(),
//                   }]
//
//     console.log(addUser);
//
//     let v = this.state;
//
//     // const submitable = v.amount.length > 0 && v.crypto.lenght > 0 && re!== null && v.email.lenght > 0;
//     //
//     //
//     //   submitable === true ? this.setState({isSubmitable:true}) : null;
//
//             let data = database.ref()
//                               .child('ico/landing/whitelist')
//                                 .push(addUser ,
//                                 function(error){
//                                   if(error){
//                                     console.log(error)
//                                   }else{
//                                     console.log('data has been saved')
//                                     return true;
//                                   }
//                                   return;
//                                 })
//
//               }
//
//   componentDidMount(){
//     database.ref().on('value' , (snapshot) => {
//           console.log(snapshot.val())
//         });
//
//         fetch('https://api.cryptonator.com/api/full/eth-usd').then(
//          function(response){
//            return response.json();
//          }
//       ).then(data =>{
//         console.log(data.ticker.price);
//         data.error ? this.setState({error:'error'}) :
//         this.setState({ change: data.ticker.price})
// });
//   }


  render() {

    return (
      <Router history={history}>

            <Switch>

                    <Route exact path={"/"}  render={(props) =>{
                    return <Main {...props}/>}}/>

                    <Route exact path={"/team"} render={(props) =>{
                    return <Main {...props}/>}}/>

                    <Route exact path={"/contacts"} render={(props) =>{
                    return <Main {...props}/>}}/>

                    <Route exact path={"/contacts/join"} render={(props) =>{
                    return <Main {...props}/>}}/>

                    <Route exact path={"/Auth"} render={(props) =>{
                    return <Login {...props}/>}}/>

                    <Route exact path={"/Auth/login"} render={(props) =>{
                    return <Login {...props}/>}}/>

                    <Redirect to="/" />

                    <Route component={Main}/>
            </Switch>

      </Router>
    );
  }
}

export default App;
