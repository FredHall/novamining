import firebase from 'firebase';

const config = {
   apiKey: "AIzaSyA9R0GhnWiIx9gC4mAmlByEKKp2XfrOdn0",
   authDomain: "novamining-ico.firebaseapp.com",
   databaseURL: "https://novamining-ico.firebaseio.com",
   projectId: "novamining-ico",
   storageBucket: "novamining-ico.appspot.com",
   messagingSenderId: "445170551376"
}

firebase.initializeApp(config);

export default firebase;

export const database = firebase.database();
